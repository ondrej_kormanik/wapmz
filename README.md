### Semestrální projekt do Responzivní webové stránky KIP/WAPMZ
Vytvořte vlastní webové stránky, které budou reprezentovat Vaše zájmy, koníčky, případně Vaši vlastní tvůrčí nebo podnikatelskou činnost. K tvorbě responzivního webu využijte komponenty frameworku Bootstrap.

Součástí semestrálního projektu bude vytvoření wireframe úvodní strany webových stránek při zobrazení na mobilním zařízení (typický smartphone neboli chytrý mobilní telefon). Jednotlivé prvky wireframe (struktury webu) stručně popište.

## Webové stránky budou obsahovat tyto součásti:

* logo, případně vhodně nastylovaný textový název projektu v horní části stránky
* 5 - 7 položek menu odkazujících na jednotlivé funkční stránky s reálným obsahem (textovým, případně i vizuálním)
* vhodné použití a umístění (nejlépe pod logem a menu, jinak vhodně v horní části stránky) komponenty Carousel se zobrazením několika rotujících obrázků, případně spolu s textovým sdělením zobrazeným přes tyto obrázky - JavaScript - Carousel
* drobečkovou navigaci, např. Úvod - O nás, apod.
* vlastní komplexní formulář - viz níže
* tabulku s alespoň 50 záznamy vytvořenou na základě komponenty DataTable spolu se stránkováním, řazením a vyhledáváním - prezentované ve Video MediaSite 16 - pokročilá práce s tabulkou, integrace řazení dat, filtrování a stránkování URL, kapitola Postup tvorby responzivního webu ve frameworku Bootstrap
* několik odkazů a tlačítek s použitím různých stylů - sekce CSS - Buttons, využití ikon Glyphicons v položkách menu nebo u tlačítek - Components - Glyphicons

* zobrazení tzv. modálního okna se zobrazením detailních informací libovolného typu - JavaScript - Modal

* zobrazení alespoň 3 záložek s libovolným obsahem a zobrazením obsahu záložky po kliknutí - JavaScript - Tab
* libovolné další komponenty nebo součásti, které používáte při tvorbě webových stránek

## Součástí komplexního responzivního formuláře bude:

* členění formuláře pomocí komponenty Panel (Components Panel) do tří sekcí: Základní údaje, Další údaje, Kontaktní údaje
* v sekci Základní údaje budou položky Název, Kód, Cena, Zařazení (výběr z několika kategorií, tvořeno pomocí elementu <select>, doplňte vlastní libovolné kategorie)
* v sekci Další údaje budou položky Popis, Rozměry, Dostupnost (zaškrtávací pole skladem, není skladem, na dotaz)
* v sekci Kontaktní údaje budou položky Jméno, Příjmení, Email, Telefon, Ulice, Město,PSČ
* v dolní části formuláře bude tlačítko Uložit
* využijte tipy a způsob rozložení formulářových polí dostupné v sekcích CSS - Forms a Components - Input Groups
Výslednou webovou stránku spolu s dalšími potřebnými soubory zabalte do souboru typu ZIP a odevzdejte.